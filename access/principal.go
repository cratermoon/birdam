package access

type Principal struct {
	attributes map[string]Attribute
}

func NewPrincipal() *Principal {
	return &Principal{
		attributes: make(map[string]Attribute),
	}
}

func (p *Principal) AddAttribute(a Attribute) {
	attrs := p.attributes
	attrs[a.ID] = a
	p.attributes = attrs
}

func (p *Principal) ContainsAttribute(key, value string) bool {
	for _, attr := range p.attributes {
		if attr.Match(key, value) {
			return true
		}
	}
	return false
}
