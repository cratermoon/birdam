package access_test

import (
	"testing"

	"gitlab.com/cratermoon/birdam/access"
)

func TestDefaultDeny(t *testing.T) {
	d := access.NewDecider()
	p := access.NewPrincipal()
	if d.IsPermitted(access.NilPolicy, *p) {
		t.Fail()
	}
}

func TestSimplePermit(t *testing.T) {
	p := access.NewPolicy()
	p.Permit("id", "steven")
	d := access.NewDecider()
	d.AddPolicy(*p)
	a := access.NewAttribute("id", "steven")
	pl := access.NewPrincipal()
	pl.AddAttribute(*a)
	if !d.IsPermitted(*p, *pl) {
		t.Fail()
	}
}
