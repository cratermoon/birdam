package access

type Attribute struct {
	ID         string
	key, value string
}

func NewAttribute(key, value string) *Attribute {
	return &Attribute{
		ID:    randString(),
		key:   key,
		value: value,
	}
}

func (a *Attribute) Match(key, value string) bool {
	return a.key == key && a.value == value
}
