package access

type Decider struct {
	Policies map[string]*Policy
}

func NewDecider() *Decider {
	return &Decider{
		Policies: make(map[string]*Policy),
	}
}

func (d *Decider) IsPermitted(py Policy, pl Principal) bool {
	policy := d.Policies[py.ID]
	if policy != nil {
		return py.Check(pl)
	}
	return false
}

func (d *Decider) AddPolicy(p Policy) {
	policies := d.Policies
	policies[p.ID] = &p
	d.Policies = policies
}
