package access

type Policy struct {
	ID          string
	Permissions map[string]string
}

var NilPolicy = Policy{}

func NewPolicy() *Policy {
	return &Policy{
		ID:          randString(),
		Permissions: make(map[string]string),
	}
}

func (p *Policy) Permit(key, value string) {
	perms := p.Permissions
	perms[key] = value
	p.Permissions = perms
}

func (p *Policy) Check(pl Principal) bool {
	for key, value := range p.Permissions {
		if pl.ContainsAttribute(key, value) {
			return true
		}
	}
	return false
}
