# Birdam – A Capability and Attribute Based Access Control Demonstrator [![GoDoc](https://godoc.org/gitlab.com/cratermoon/birdam?status.svg)](https://godoc.org/gitlab.com/cratermoon/birdam) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/cratermoon/birdam)](https://goreportcard.com/report/gitlab.com/cratermoon/birdam)

An exploration into marrying CBAC and ABAC

## Getting Started

These instructions will get you a copy of the project up and running
on your local machine for development and testing purposes. See
deployment for notes on how to deploy the project on a live system.

### Prerequisites

First install [Go](http://golang.org), at least 1.13.

### Installing
 
If you just want to install the binary to your current directory and don't care about the source code, run

```bash
GOBIN="$(pwd)" GOPATH="$(mktemp -d)" go get $PROJECT_URL
```

## Running the tests

```bash
go test ./...
```

## Deployment



## Built With

* [echo](https://echo.labstack.com/) – High performance, minimalist Go web framework

## Contributing

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/cratermoon/birdam/tags). 

## Authors

* **Steven E. Newton** - *Initial work* - [cratermoon](https://gitlab.com/cratermoon)


## License

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This project is licensed under the GPL v3 License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Inspiration

